# Lines configured by zsh-newuser-install
 HISTFILE=~/.histfile
 HISTSIZE=1000
 SAVEHIST=1000
 setopt autocd
 unsetopt beep

# The following lines were added by compinstall
 zstyle :compinstall filename '/home/siki/.zshrc'
 autoload -Uz compinit
 compinit

# Vim mode
 bindkey -v
 export KEYTIMEOUT=1

# Zsh vi mode plugin
source /usr/share/zsh/plugins/zsh-vi-mode/zsh-vi-mode.plugin.zsh

# Escape mode key
 ZVM_VI_INSERT_ESCAPE_BINDKEY=^[l

#zstyle ':complition:*' menuselect = 2
 zmodload zsh/complist

# Use vim keys in tab complete menu:
 bindkey -M menuselect 'h' vi-backward-char
 bindkey -M menuselect 'k' vi-up-line-or-history
 bindkey -M menuselect 'l' vi-forward-char
 bindkey -M menuselect 'j' vi-down-line-or-history
 bindkey -v '^?' backward-delete-char

# Starship initialization
 eval "$(starship init zsh)"

# FZF initializations
[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh
 export FZF_DEFAULT_OPS="--extended"
 export FZF_DEFAULT_COMMAND="rg --files --no-ignore-vcs --follow -g'!node_modules/' -g'!.git' -g'!pycache'"
 export FZF_CTRL_T_COMMAND="$FZF_DEFAULT_COMMAND"
 export FZF_COMPLETION_TRIGGER='~~'

# Custom key bindings for fzf
# bindkey -s '^[f' 'nvim **\t\n'
 bindkey -s '\ef' 'nvim **\t\n'

# Spicetify for spotify custiom design
 export SPICETIFY_INSTALL='/home/.local/bin/'

# Default Editor
 export EDITOR="nvim"

# Virtual env wrapper
 source /home/siki/.local/bin/virtualenvwrapper.sh

# Adding path
 export PATH="${PATH}:/home/siki/.local/bin"

# Cuda path for tensorflow gpu
 export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/opt/cuda/extras/CUPTI/lib64:/opt/cuda/lib64

# Aliases
 alias vim="nvim"
 alias zshconfig="vim ~/.zshrc"
 alias vimconfig="vim ~/.config/nvim/settings.vim"
 alias refresh="sudo systemctl restart NetworkManager"
 alias crack="sudo airmon-ng start wlp3s0" 
 alias scan="sudo airodump-ng wlp3s0mon"
 alias stop="sudo airmon-ng stop wlp3s0mon"
 alias fuck="shutdown 0"
 alias ls="lsd"
 alias cl="clear"
 #alias config='/usr/bin/git --git-dir=$HOME/.cfg/ --work-tree=$HOME'
 alias config='/usr/bin/git --git-dir=$HOME/.dotfiles --work-tree=$HOME'
 alias ..="cd .."
 alias gitjk="history 10 | tac | gitjk_cmd"
 alias refresh-mirrors="sudo reflector --verbose --latest 10 --sort rate --protocol https --download-timeout 30 --country 'China, India, Japan, Korea' --save /etc/pacman.d/mirrorlist"

# Sys information
 #pfetch
