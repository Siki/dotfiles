" source ~/.config/nvim/plugins.vim


lua << EOF
    require("plugins")
    require('settings')
    require('custom')

    require("plug-config.coc")
    require("plug-config.nerdtree")
    require("plug-config.fzf")
    require("plug-config.quickscope")
    require("plug-config.airline")
    require("plug-config.prettier")
    require("plug-config.vim-sneak")

EOF

" source ~/.config/nvim/plug-config/coc.vim
"source ~/.config/nvim/plug-config/quickscope.vim
" source ~/.config/nvim/plug-config/fzf.vim
" source ~/.config/nvim/plug-config/vim-sneak.vim
