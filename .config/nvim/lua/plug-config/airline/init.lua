local vim = vim

-- Enables powerline font
vim.g.airline_powerline_fonts = 1

-- Theme
-- vim.o.g:airline_theme='base16_gruvbox_dark_hard'
