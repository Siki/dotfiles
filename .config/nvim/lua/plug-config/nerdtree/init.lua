local vim = vim
local api = vim.api

vim.g.NERDDefaultAlign = 'left'

-- Adds spaces after the comment delimeters by default
vim.g.NERDSpaceDelims = 1

vim.g.NERDCompactSexyComs = 1
-- Toggle nerd tree
api.nvim_set_keymap("n", "<leader>i", ":NERDTreeToggle <CR>", {noremap = true})
