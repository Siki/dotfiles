local opts = {noremap = true}
local vim = vim
local api = vim.api

-- Search for mappings
api.nvim_set_keymap("n", "<leader><tab>", "<plug>(fzf-maps-n)", opts)
api.nvim_set_keymap("x", "<leader><tab>", "<plug>(fzf-maps-x)", opts)
api.nvim_set_keymap("o", "<leader><tab>", "<plug>(fzf-maps-o)", opts)

-- Insert mode completion
api.nvim_set_keymap("i", "<c-x><c-w>", "<plug>(fzf-complete-word)", opts)
api.nvim_set_keymap("i", "<c-x><c-f>", "<plug>(fzf-complete-path)", opts)
api.nvim_set_keymap("i", "<c-x><c-l>", "<plug>(fzf-complete-line)", opts)


-- Normal mode mappings
-- One key keymaps
api.nvim_set_keymap("n", "<leader>;", ":Files<CR>", opts)
api.nvim_set_keymap("n", "<leader>l", ":Buffers <CR>", opts)

-- Two key keymaps
api.nvim_set_keymap("n", "<leader>ff", ":Files ~<CR>", opts)
api.nvim_set_keymap("n", "<leader>fr", ":Rg<CR>", opts)
api.nvim_set_keymap("n", "<leader>fc", ":Colors <CR>", opts)
api.nvim_set_keymap("n", "<leader>fw", ":Windows <CR>", opts)

