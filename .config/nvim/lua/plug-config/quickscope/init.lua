local vim = vim
local api = vim.api

-- Trigger a highlight in the appropriate direction when pressing these keys:
vim.g.qs_highlight_on_keys = { 'f', 'F', 't', 'T' }
vim.g.qs_max_chars=150

-- Custom colors
api.nvim_command([[
    highlight QuickScopePrimary guifg='#8abeb7' gui=underline ctermfg=81 cterm=underline
    highlight QuickScopeSecondary guifg='#8abeb7' gui=underline ctermfg=196 cterm=underline
]])
