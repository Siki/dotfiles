local vim = vim
local api = vim.api

-- Sets the leader as space
vim.g.mapleader = " "

-- Turns the syntax highlighting on
vim.o.syntax = "on"

-- vim.o.encoding=utf8
-- Don't want case sensitive searches
vim.o.ignorecase = true

-- If a upper case characteer is typed then do a case sensitive
vim.o.smartcase = true

-- Adds the indent to new line automatically when needed
vim.o.smartindent = true

-- Wraps the text if the text goes beyond the window width
vim.o.wrap = true

-- Specifies the undo directory
-- vim.o.undodir="~/.config/nvim/undodir"

-- Maintains the undo history between the sessions
vim.o.undofile = false

-- Don't make a swapfile
vim.o.swapfile = false

-- Shows the matched pattern of search expression while typing the text (while typing the output is dynamically showing)
vim.o.incsearch = true

-- Highlights the line where the cursor is currently
vim.o.cursorline = true

-- Enables the mouse functions in all modes
vim.o.mouse="a"

-- Copy the system clipboard to vim clipboard
vim.o.clipboard = "unnamedplus"

-- Tab configs
vim.o.softtabstop=4
vim.o.shiftwidth=4
vim.o.tabstop=4
-- vim.o.expandtab = true

-- Shows the trailing spaces and tabs
vim.opt.listchars.tab = ".."
vim.opt.listchars.nbsp = "␣"
vim.opt.listchars.trail = "•"
vim.opt.listchars.extends = ">"
vim.opt.listchars.precedes = "<"

-- Enables all the above mentioned list chars features
vim.opt.list = true

-- Enables 24 bit RGB colors
vim.opt.termguicolors = true

-- Sets the create backup to false
vim.opt.backup = false
vim.opt.writebackup = true

-- Relative line numbering
-- Number and Relativenumber are window options
vim.wo.relativenumber = true

-- but we don't want pure relative line numbering. The line where the cursor is
-- should show absolute line number
vim.wo.number = true

-- Sets the colorscheme
vim.cmd [[
    colorscheme onedark
]]

-- Remapping
-- api.nvim_set_keymap("n", "]h", "<Plug>(GitGutterNextHunk)", {noremap = true})
-- api.nvim_set_keymap("n", "[h", "<Plug>(GitGutterPrevHunk)", {noremap = true})
-- api.nvim_set_keymap("n", "<leader>gs", ":G<CR>", {noremap = true})

vim.g.python_highlight_all = 1
vim.g.javascript_plugin_jsdoc = 1
vim.g.user_emmet_mode='a'    -- enable all function in all mode.

