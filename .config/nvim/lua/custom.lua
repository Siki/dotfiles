local M = {}

-- To remove the warnings 
local vim = vim

-- nnoremap specifies do not expand the key bindings if the specified key bindings
-- are already mapped to other keys (we don't want the keymap we set to bind to other keys in recursive manner)
local keymap = vim.api.nvim_set_keymap
local opts = {noremap = true, silent = true}


-- Saves the file if the file is modified
function M.writeIfModified()
    if vim.bo.modifiable and not vim.bo.readonly and vim.bo.modified then
        vim.cmd [[write]]
    end
end

-- Selects the next buffer file
function M.nextBuffer()
    M.writeIfModified()
    vim.cmd([[bnext]])
end

-- Selects the previous buffer file
function M.prevBuffer()
    M.writeIfModified()
    vim.cmd [[bprevious]]
end


-- Removes the highlight
keymap("n", "<M-f>", ":noh<CR>" ,opts)

-- Toggles the nerd tree
keymap("n", "<leader>i", ":NERDTreeToggle <CR>" ,opts)

-- Remaps the tab and shift tab to change between next buffer and previous buffer
vim.keymap.set("n", "<tab>", M.nextBuffer ,opts)
vim.keymap.set("n", "<s-tab>", M.prevBuffer ,opts)

-- Hormizontal and vertical split
keymap("n", "<leader>v", ":vsplit <CR>" ,opts)
keymap("n", "<leader>h", ":split <CR>" ,opts)

-- Prettier
keymap("n", "<leader>p", ":Prettier" ,opts)

-- Movement on insert mode
keymap("i", "<M-a>", "<Left>", opts)
keymap("i", "<M-d>", "<Right>", opts)
keymap("i", "<M-w>", "<ESC>wi", opts)
keymap("i", "<M-b>", "<ESC>bi", opts)
keymap("i", "<C-l>", "<Right>", opts)
keymap("i", "<C-h>", ":Files<CR>", opts)

keymap("v", "<s-Y>", '"+y', opts)

-- Remap $ to shift-e
keymap("n", "<s-E>", '$', opts)

-- Operator mapping $ to shift-e
keymap("o", "<s-E>", '$', opts)

-- Remap ^ to shift-b
keymap("n", "<s-B>", '^', opts)

-- Operator mapping ^ to shift-b
keymap("o", "<s-B>", '^', opts)


-- Move the texts on the line
keymap("v", "J", ":m '>+1<CR>gv=gv", opts)
keymap("v", "K", ":m '<-2<CR>gv=gv", opts)
keymap("i", "<C-j>", "<esc>:m .+1<CR>==i", opts)
keymap("i", "<C-k>", "<esc>:m .-2<CR>==i", opts)
keymap("n", "<leader>j", ":m .+1<CR>==", opts)
keymap("n", "<leader>k", ":m .-2<CR>==", opts)

-- Jsdoc
keymap("n", "<C-l>", "<Plug>(jsdoc)", opts)


-- Harpoon
keymap("n", "hm", ":lua require('harpoon.mark').add_file()<CR>", opts)
keymap("n", "hg", ":lua require('harpoon.ui').toggle_quick_menu()<CR>", opts)


-- vim.api.nvim_exec(

-- [[
--     " nnoremap specifies do not expand the key bindings if the specified key bindings
--     " are already mapped to other keys (we don't want the keymap we set to bind to other keys in recursive manner)

--     " Remove highlight
--     " "nnoremap <M-f> :noh <CR>

--     " Nerd jree
--     " "nnoremap <leader>i :NERDTreeToggle<CR>

--     " Tab Change
--     " nnoremap  <silent>   <tab>  :if &modifiable && !&readonly && &modified <CR> :write<CR> :endif<CR>:bnext<CR>
--     nnoremap  <silent> <s-tab>  :if &modifiable && !&readonly && &modified <CR> :write<CR> :endif<CR>:bprevious<CR>

--     " Tab Split
--     nmap <leader>v :vsplit <CR>
--     nmap <leader>h :split <CR>

--     " Preetier
--     " nnoremap <leader>p :CocCommand prettier.formatFile <CR>
--     nnoremap <leader>p :Prettier <CR>

--     " Imap
--     inoremap <M-i> <ESC>I
--     inorema <M-a> <ESC>A
--     inoremap <M-d> <ESC>ea
--     inoremap <M-s> <ESC>bi

--     " Paste from external clipboard
--     " "nnoremap <S-P> "+p

--     " Copy to external clipboard
--     vnoremap <S-Y> "+y <CR>

--     " Remap $ to shift-e
--     nnoremap <S-E> $

--     " Operator mapping $ to shift-e
--     onoremap <S-E> $

--     " Remap ^ to shift-b
--     nnoremap <S-B> ^

--     " Operator mapping ^ to shift-b
--     onoremap <S-B> ^

--     " move rext
--     " gv -> rehighlight = -> manage indent so, rehighlight indent rehighlight
--     vnoremap J  :m '>+1<CR>gv=gv
--     vnoremap K  :m '<-2<CR>gv=gv
--     inoremap <C-j> <esc>:m .+1<CR>==i
--     inoremap <C-k> <esc>:m .-2<CR>==i
--     nnoremap <leader>j :m .+1<CR>==
--     nnoremap <leader>k :m .-2<CR>==

--     " Jsdoc
--     nmap <silent> <C-l> <Plug>(jsdoc)

-- ]], false)
