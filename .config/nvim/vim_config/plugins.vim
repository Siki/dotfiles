call plug#begin('~/.config/nvim/plugged')
    " For nerdtree and nerdtree icon
    Plug 'preservim/nerdtree'

    " Commet made easier
    Plug 'scrooloose/nerdcommenter'

    " Auto close brackets/quotations
    Plug 'jiangmiao/auto-pairs'

    " Autocompletion
    Plug 'neoclide/coc.nvim', {'branch': 'release'}

    " Colorscheme
    " Plug 'morhetz/gruvbox'
    " Plug 'crusoexia/vim-dracula'
    Plug 'dracula/vim', { 'as': 'dracula' }
    Plug 'joshdick/onedark.vim'

    " Fuzzy finder
    "Plug 'kien/ctrlp.vim'
    Plug '~/.fzf'
    Plug 'junegunn/fzf.vim'

    " GIT integrations 
    Plug 'airblade/vim-gitgutter'
    Plug 'tpope/vim-fugitive'

    " startify
    Plug 'mhinz/vim-startify'

    " status line
    Plug 'vim-airline/vim-airline'
    Plug 'vim-airline/vim-airline-themes'

    " quick scope
    Plug 'unblevable/quick-scope'

    " surround vim
    Plug 'tpope/vim-surround'

    " typescript syntax
    Plug 'HerringtonDarkholme/yats.vim'

    " indent
    "Plug 'Yggdroot/indentLine'

    " python syntax
    Plug 'vim-python/python-syntax'
    Plug 'pangloss/vim-javascript'

    " vim dev icons
    Plug 'ryanoasis/vim-devicons'

	" Windows management
	Plug 'nvim-lua/plenary.nvim'
	Plug 'nvim-lua/popup.nvim'

	" Vim sneak for search
	Plug 'justinmk/vim-sneak'

	" Copilot

    " Snippets
    Plug 'neoclide/coc-snippets'
    Plug 'honza/vim-snippets'

    " Prettier
    " post install (yarn install | npm install) then load plugin only for editing supported files
    Plug 'prettier/vim-prettier', { 'do': 'yarn install --frozen-lockfile --production' }

    " Emmet
    " Plug 'mattn/emmet-vim'

    " Jsdoc
    Plug 'heavenshell/vim-jsdoc', {
      \ 'for': ['javascript', 'javascript.jsx','typescript'],
      \ 'do': 'make install'
    \}

call plug#end()

