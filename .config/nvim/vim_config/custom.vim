" Remove highlight
nnoremap <M-f> :noh <CR>

" Nerd jree
" nnoremap <leader>i :NERDTreeToggle<CR>

" Tab Change
nnoremap  <silent>   <tab>  :if &modifiable && !&readonly && &modified <CR> :write<CR> :endif<CR>:bnext<CR>
nnoremap  <silent> <s-tab>  :if &modifiable && !&readonly && &modified <CR> :write<CR> :endif<CR>:bprevious<CR>

" Tab Split
nmap <leader>v :vsplit <CR>
nmap <leader>h :split <CR>

" Preetier
" nnoremap <leader>p :CocCommand prettier.formatFile <CR>
nnoremap <leader>p :Prettier <CR>

" Imap
inoremap <M-i> <ESC>I
inorema <M-a> <ESC>A
inoremap <M-d> <ESC>ea
inoremap <M-s> <ESC>bi

" Paste from external clipboard
nnoremap <S-P> "+p

" Copy to external clipboard
vnoremap <S-Y> "+y <CR>

" Remap $ to shift-e
nnoremap <S-E> $

" Operator mapping $ to shift-e
onoremap <S-E> $

" Remap ^ to shift-b
nnoremap <S-B> ^

" Operator mapping ^ to shift-b
onoremap <S-B> ^

" move rext
" gv -> rehighlight = -> manage indent so, rehighlight indent rehighlight
vnoremap J  :m '>+1<CR>gv=gv
vnoremap K  :m '<-2<CR>gv=gv
inoremap <C-j> <esc>:m .+1<CR>==i
inoremap <C-k> <esc>:m .-2<CR>==i
nnoremap <leader>j :m .+1<CR>==
nnoremap <leader>k :m .-2<CR>==

" Jsdoc
nmap <silent> <C-l> <Plug>(jsdoc)
