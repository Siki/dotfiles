let mapleader = " "
let g:python_host_prog = '/usr/bin/python' 
syntax on

set number relativenumber
set encoding=utf8
set noerrorbells
set smartindent
set wrap
set noswapfile
set nobackup
set undodir=~/.config/nvim/undodir
set undofile
set incsearch" 
set cursorline
set mouse=a
set listchars=tab:..\ ,nbsp:␣,trail:•,extends:⟩,precedes:⟨
set list
set termguicolors
set clipboard+=unnamedplus

" Tab configs
" To show space and tabls with characters use the following command
"set listchars=space:_,train:.
"set listchars=space:_\,tab:>,trail:·,extends:⟩,precedes:⟨
set softtabstop=4
set shiftwidth=4
set tabstop=4
set expandtab
"set nowrap

"colorscheme gruvbox
"colorscheme dracula
colorscheme onedark

" Remapping
nmap ]h <Plug>(GitGutterNextHunk)
nmap [h <Plug>(GitGutterPrevHunk)

nmap <leader>gs : G<CR>

" Auto running programs
autocmd filetype python nnoremap <F5> :w <bar> exec '!python '.shellescape('%')<CR>
autocmd filetype c nnoremap <F5> :w <bar> exec '!gcc '.shellescape('%').' -o '.shellescape('%:r').' && ./'.shellescape('%:r')<CR>
autocmd filetype cpp nnoremap <F5> :w <bar> exec '!g++ '.shellescape('%').' -o '.shellescape('%:r').' && ./'.shellescape('%:r')<CR>
autocmd filetype ts nnoremap <F5> :w <bar> exec '!tsc %'<CR>
autocmd filetype vim nnoremap <F5> :w <bar> :source % <CR>
autocmd filetype sh nnoremap <F5> :w <bar> exec '!./%' <CR>


let g:NERDDefaultAlign = 'left'
let g:python_highlight_all = 1
let g:javascript_plugin_jsdoc = 1
let g:airline_powerline_fonts = 1
let g:airline_theme='base16_gruvbox_dark_hard'
let g:dracula_italic = 1
let g:user_emmet_mode='a'    "enable all function in all mode.



" Prettier
let g:prettier#autoformat = 1
let g:prettier#autoformat_require_pragma = 0
" when running at every change you may want to disable quickfix
"let g:prettier#quickfix_enabled = 0
"autocmd TextChanged,InsertLeave *.js,*.jsx,*.mjs,*.ts,*.tsx,*.css,*.less,*.scss,*.json,*.graphql,*.md,*.vue,*.svelte,*.yaml,*.html PrettierAsync
