" Trigger a highlight in the appropriate direction when pressing these keys:
let g:qs_highlight_on_keys = ['f', 'F', 't', 'T']

highlight QuickScopePrimary guifg='#8abeb7' gui=underline ctermfg=81 cterm=underline
highlight QuickScopeSecondary guifg='#8abeb7' gui=underline ctermfg=196 cterm=underline

let g:qs_max_chars=150
