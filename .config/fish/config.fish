﻿starship init fish | source
set fish_greeting

# Sets the vim key bindings as default
fish_vi_key_bindings

# Set the cursor shapes for the different vi modes.
set fish_cursor_default     block      blink
set fish_cursor_insert      line       blink
set fish_cursor_replace_one underscore blink
set fish_cursor_visual      block

# Aliases
 alias vim "nvim"
 alias zshconfig "vim ~/.zshrc"
 alias ohmyzsh "vim ~/.oh-my-zsh"
 alias fishconfig "vim ~/.config/fish/config.fish"
 alias vimconfig "vim ~/.config/nvim/settings.vim"
 alias refresh "sudo systemctl restart NetworkManager"
 alias crack "sudo airmon-ng start wlp3s0" 
 alias scan "sudo airodump-ng wlp3s0mon"
 alias stop "sudo airmon-ng stop wlp3s0mon"
 alias fuck "shutdown 0"
 alias ls "lsd"
 alias cl "clear"
 alias gitadog "git log --all --decorate --oneline --graph --color=always"
 alias .. "cd .."
 alias config '/usr/bin/git --git-dir=$HOME/.dotfiles --work-tree=$HOME'
 alias gitjk "history 10 | tac | gitjk_cmd"
 alias refresh-mirrors "sudo reflector --verbose --latest 10 --sort rate --protocol https --download-timeout 30 --country 'China, India, Japan, Korea' --save /etc/pacman.d/mirrorlist"
 alias update "sudo pacman -Syu && paru -Syu --noconfirm"
 alias icat "kitty +kitten icat"
 alias conf "vim ~/.config/fish/config.fish"
 alias i3conf "vim ~/.config/i3/config"

 alias sshdev21 "ssh -i ~/.ssh/ayush.musyaju -f ayush.musyaju@devqa21.vroozi.com -L 8080:localhost:8080 -N &&
 ssh -i ~/.ssh/ayush.musyaju -f ayush.musyaju@devqa21.vroozi.com -L 8251:localhost:8251 -N &&
 ssh -i ~/.ssh/ayush.musyaju -f ayush.musyaju@devqa21.vroozi.com -L 8885:localhost:8885 -N &&
 ssh -i ~/.ssh/ayush.musyaju -f ayush.musyaju@devqa21.vroozi.com -L 8580:localhost:8580 -N "

 alias sshdev28 "ssh -i ~/.ssh/ayush.musyaju -f ayush.musyaju@devqa28.vroozi.com -L 8080:localhost:8080 -N &&
 ssh -i ~/.ssh/ayush.musyaju -f ayush.musyaju@devqa28.vroozi.com -L 8251:localhost:8251 -N &&
 ssh -i ~/.ssh/ayush.musyaju -f ayush.musyaju@devqa28.vroozi.com -L 8885:localhost:8885 -N &&
 ssh -i ~/.ssh/ayush.musyaju -f ayush.musyaju@devqa28.vroozi.com -L 8580:localhost:8580 -N"

 alias sshdev32 "ssh -i ~/.ssh/ayush.musyaju -f ayush.musyaju@devqa32.vroozi.com -L 8080:localhost:8080 -N &&
 ssh -i ~/.ssh/ayush.musyaju -f ayush.musyaju@devqa32.vroozi.com -L 8251:localhost:8251 -N &&
 ssh -i ~/.ssh/ayush.musyaju -f ayush.musyaju@devqa32.vroozi.com -L 8885:localhost:8885 -N &&
 ssh -i ~/.ssh/ayush.musyaju -f ayush.musyaju@devqa32.vroozi.com -L 8580:localhost:8580 -N "

 alias killports "fuser -k 8080/tcp &&
 fuser -k 8251/tcp &&
 fuser -k 8885/tcp &&
 fuser -k 8580/tcp"

 #alias config '/usr/bin/git --git-dir=$HOME/.cfg/ --work-tree=$HOME'

# Sys information
 #pfetch

# FZF
export FZF_DEFAULT_OPS="--extended"
export FZF_DEFAULT_COMMAND="rg --files --no-ignore-vcs --smart-case --no-heading --follow -g'!node_modules/' -g'!.git' -g'!pycache'"
set FZF_CTRL_T_COMMAND "$FZF_DEFAULT_COMMAND"
export FZF_COMPLETION_TRIGGER='~~'

# Bindings
bind \cl 'clear;'

# System information
#pfetch

# Default editor
export EDITOR="nvim"
export PATH="$PATH:/home/siki/.local/bin:/usr/share/man"
export PATH="$PATH:/opt/sonar-scanner/bin"

# Cuda path for tensorflow gpu
export LD_LIBRARY_PATH="$LD_LIBRARY_PATH:/opt/cuda/extras/CUPTI/lib64:/opt/cuda/lib64"

# Doom emacs path
export PATH="$PATH:/home/siki/.emacs.d/bin"

# Yarn path
export PATH="$PATH:/home/siki/.yarn/bin"

# Displays random color scripts
#colorscript -r

# [ -s $HOME/.nvm/nvm.sh ] && . $HOME/.nvm/nvm.sh
# source /usr/share/nvm/init-nvm.sh

export G_MESSAGES_DEBUG=all



